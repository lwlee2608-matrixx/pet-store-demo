package com.lwlee2608.petstoredemo.controller;

import com.lwlee2608.petstoredemo.api.PetApi;
import com.lwlee2608.petstoredemo.model.Pet;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/pets")
public class PetController implements PetApi {

    private Map<String, Pet> cache = new ConcurrentHashMap<>();

    @PostMapping("")
    public ResponseEntity<Pet> create(@Valid @RequestBody Pet pet) {
        String id = UUID.randomUUID().toString();
        cache.put(id, pet.setId(id));
        return ResponseEntity
                .created(ServletUriComponentsBuilder
                        .fromCurrentContextPath().path("/pets/{id}")
                        .buildAndExpand(id)
                        .toUri())
                .body(pet);
    }

    @GetMapping(value="")
    public ResponseEntity<List<Pet>> getByName (
            @RequestParam(value = "name", required = true) String name) {
        return ResponseEntity
                .ok()
                .body(cache.values()
                        .stream()
                        .filter( p -> p.getName().equals(name))
                        .collect(Collectors.toList()));

    }

    @GetMapping("/{id}")
    public ResponseEntity<Pet> getById(@PathVariable String id) {
        Pet pet = cache.get(id);
        if (pet == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(pet);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id) {
        cache.remove(id);
        return ResponseEntity.noContent().build();
    }
}
