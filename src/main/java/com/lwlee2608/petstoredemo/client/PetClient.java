package com.lwlee2608.petstoredemo.client;

import com.lwlee2608.petstoredemo.api.PetApi;
import com.lwlee2608.petstoredemo.model.Pet;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

public class PetClient implements PetApi {

    private final RestTemplate restTemplate = new RestTemplate();
    private final String url;

    public PetClient (String url) {
        this.url = url;
    }

    public ResponseEntity<Pet> create(Pet pet) {
        return restTemplate.exchange(
                UriComponentsBuilder.fromUriString(url)
                        .path("/pets")
                        .build().toUri(),
                HttpMethod.POST,
                new HttpEntity<>(pet),
                Pet.class);
    }

    public ResponseEntity<List<Pet>> getByName (String name) {
        return restTemplate.exchange(
                UriComponentsBuilder.fromUriString(url)
                        .path("/pets")
                        .queryParam("name", name)
                        .build().toUri(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Pet>>(){});
    }

    public ResponseEntity<Pet> getById(String id) {
        return restTemplate.exchange(
                UriComponentsBuilder.fromUriString(url)
                        .path("/pets/{id}")
                        .buildAndExpand(id).toUri(),
                HttpMethod.GET,
                null,
                Pet.class);
    }

    public ResponseEntity<Void> delete(String id) {
        return restTemplate.exchange(
                UriComponentsBuilder.fromUriString(url)
                        .path("/pets/{id}")
                        .buildAndExpand(id).toUri(),
                HttpMethod.DELETE,
                null,
                Void.class);
    }
}
