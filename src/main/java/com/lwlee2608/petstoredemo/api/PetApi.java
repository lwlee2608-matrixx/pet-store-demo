package com.lwlee2608.petstoredemo.api;

import com.lwlee2608.petstoredemo.model.Pet;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Pet Store Demo Api
 */
public interface PetApi {
    /**
     * <p> Create a pet record
     * </p>
     * @param pet the pet object to be created
     * @return the pet object created
     */
    ResponseEntity<Pet> create(Pet pet);

    /**
     * <p> Get pet by name
     * </p>
     * <p> Name should be a query parameter
     * </p>
     * @param name the name of the pet
     * @return the pet that match the name
     */
    ResponseEntity<List<Pet>> getByName (String name);
    /**
     * <p> Get pet by id
     * </p>
     * @param id the id of the pet
     * @return the pet that match the id
     */
    ResponseEntity<Pet> getById(String id);
    /**
     * <p> Delete pet that match the id
     * </p>
     * @param id the id of the pet
     * @return void
     */
    ResponseEntity<Void> delete(String id);
}
