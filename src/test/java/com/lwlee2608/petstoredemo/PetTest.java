package com.lwlee2608.petstoredemo;

import com.lwlee2608.petstoredemo.api.PetApi;
import com.lwlee2608.petstoredemo.client.PetClient;
import com.lwlee2608.petstoredemo.model.Pet;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class PetTest {
    
    private static final Logger logger = LoggerFactory.getLogger(PetTest.class);

    PetApi petApi = new PetClient("http://localhost:8080");

    @Ignore
    @Test
    public void testCRUD() {
        ResponseEntity<Pet> createResponse = petApi.create(new Pet()
                .setName("Max")
                .setAge(3));
        logger.info("create pet : {}", createResponse);
        String id = createResponse.getBody().getId();

        {
            String name = "Max";
            ResponseEntity<List<Pet>> response = petApi.getByName(name);
            logger.info("get by name {} : {}", name, response);
        }
        {
            ResponseEntity<Pet> response = petApi.getById(id);
            logger.info("get by id {} : {}", id, response);
        }
        {
            ResponseEntity<Void> response = petApi.delete(id);
            logger.info("delete : {}", response);
        }
        {
            ResponseEntity<Pet> response = petApi.getById(id);
            logger.info("get by id {} : {}", id, response);
        }
    }
}
